# 👏 Translate text to sign language app 

This is a project made to mainly implement Heroku to ReactJS. The usage of the application is to translate text to sign language.

## ✌️ Description

### 🔥 About the application

Translation to sign language app is a small application to use for translation. The application supports English text to signs in American Manual Alphabet (AMA). 

The application contains the following functions:

* Login / Create user - Stored in Heroku and localstorage.

* Component to render translations from text to sign language.

* Profile page where you can find your last 10 translations. You are able to delete them locally, but will be fetched from Heroku when Log out / Login once again.

### 🤖 Technologies

* HTML5
* CSS3
* Mantine
* JavaScript
* ReactJS
* Redux
* Heroku

## ⭐ Getting Started

### ⚠️ Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work. 

### ⚡ Installing

Follow the steps below to create and setup the translation application.

1. Get an API-key at Heroku(https://www.heroku.com/)
2. Clone the repo:
```
git clone https://git@gitlab.com:elon.mansson/Translation.git
``` 
3. Install NPM packages:
```
npm install
```
4. Create and enter your API key in .ENV
```
REACT_APP_KEY = your_api_key; ⚠️
REACT_APP_API_URL = noroff translation API URL; ⚠️
```

### 💻 Executing program

* How to run the program:
```
npm start
```

## 😎 Authors

Contributors and contact information

Elon Månsson 
[@Elon.Mansson](https://gitlab.com/elon.mansson/)
Philip Hjelmberg 
[@PhilipHjelmberg](https://gitlab.com/PhilipHjelmberg)

## 🦝 Version History

* 0.1
    * Initial Release

## 🌌 License

This project is fully free for use.
