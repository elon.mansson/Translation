import "./style.css";
import Profile from "./components/profile";
import Nav from "./components/nav";
import Signup from "./components/signup";
import Translate from "./components/translate";
import { useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getUser, updateLoggedIn, updateTranslation } from "./redux/userSlice";


function App() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  //Get all users from Redux
  const users = useSelector((state) => state.user);
  //Get data from localStorage if there is any
  const userFromStorage = JSON.parse(localStorage.getItem("my-user"));
  useEffect(() => {
    //Check the status and if it hasnt been called before it will run
    if (users.status === "idle") {
      //Fetch data from api
      dispatch(getUser());
    }
    //If it has data from API and localstorage
    if (users.userList.length > 0 && userFromStorage !== null && users.loggedIn === false) {
      //map through all users
      users.userList.map((user) => {
        //if it find a match with the name in localstorage and users from api it will update global loginstate to true
        if (userFromStorage.username === user.username.toLowerCase()) {
          
          dispatch(updateTranslation(userFromStorage.translations));
          dispatch(updateLoggedIn(true));
          navigate('/translate')
        }
      });
    }
  }, [users, dispatch]);

  // A return statement that is returning depending if the user is current loggedin or not
  return users.loggedIn ? (
    <div className="App">
      <Nav loggedIn={true} />
      <div className="hero-container">
        <Routes>
          <Route path="/profile" element={<Profile />} />
          <Route path="/translate" element={<Translate />} />
        </Routes>
      </div>
    </div>
  ) : (
    <div className="App">
      <Nav loggedIn={false} />
      <div className="hero-container">
        <Signup />
      </div>
    </div>
  )
}

export default App;
