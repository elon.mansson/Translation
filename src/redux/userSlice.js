import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
//API KEY
let apiKey = process.env.REACT_APP_KEY
let URL = process.env.REACT_APP_API_URL

//API CALL TO GET USERDATA
export const getUser = createAsyncThunk("Translation/getUser", async () => {
  const res = await axios.get(URL);
  return res.data;
});
//API CALL TO POST NEWUSER
export const addUser = createAsyncThunk("Translation/addUser", async (user) => {
  axios.post(URL, user, {
    headers: {
      'Content-Type': 'application/json',
      'X-API-Key': apiKey
    }
  });
});
//API CALL TO PATCH CURRENT USER WITH A NEW TRANSLATION
export const addUserTranslation = createAsyncThunk("Translation/addUserTranslation", async (user) => {
  await axios.patch(`${URL}/${user.id}`, {translations: user.translations}, {
    headers: {
      'Content-Type': 'application/json',
      'X-API-Key': apiKey
    }
  });
});

const slice = createSlice({
  name: "translation",
  initialState: {
    name: "initial",
    status: "idle",
    loggedIn: false,
    currentTranslationList: [],
    userList: []
  },
  reducers: {
    //Function to update the status to idle if a new user is created.
    updateStatus: (state, action) => {
      state.status = "idle";
      console.log(state.status);
    },
    //Function to change the global state if user is logged in or not.
    updateLoggedIn: (state, action) => {
      state.loggedIn = action.payload;
      console.log(state.loggedIn, action.payload);
    },
    updateTranslation: (state, action) => {
      state.currentTranslationList = action.payload
      console.log(action.payload)
    }
  },
  extraReducers: {
    [getUser.pending]: (state) => {
      state.status = "loading...";
      console.log(state.status);
    },
    [getUser.fulfilled]: (state, action) => {
      state.status = "success";
      console.log(state.status);
      state.userList = action.payload;
    },
    [getUser.rejected]: (state) => {
      state.status = "rejected";
      console.log(state.status);
    },
  },
});

const { actions, reducer } = slice;

export const { updateStatus, updateLoggedIn, getTotal, updateTranslation } = actions;

export default reducer;

