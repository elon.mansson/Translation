import { useState, useEffect } from "react";
import TranslationItem from "./translationItem";
import { useDispatch, useSelector } from "react-redux";
import { updateTranslation } from "../redux/userSlice";

const Profile = () => {
    //Get current user list of translations
    const users = useSelector((state) => state.user.currentTranslationList);
    //A state to use to update the DOM
    const [userList, setUserList] = useState([])
    //The current user from localStorage
    const userFromStorage = JSON.parse(localStorage.getItem("my-user"));
    const dispatch = useDispatch()
    
    useEffect(() => {
        //Controll if there is any translations in the current user. It also check so there is a translation in localstorage
        if(users.length === 0 && userFromStorage.translation !== undefined){
                //Update global state with a list of current users translations
                dispatch(updateTranslation(userFromStorage.translations))
                //Update the dom
                setUserList(users)       
        } else {
            //If the user have any current translation update the DOM
                setUserList(users)
            }
    }, [users])

    return (
        <div className="profile-container hero-component">
            <h1>Profile</h1>
            {users?.length !== 0 ? userList?.map((translation, i) => {
                //Only return the last 10 object from the array.
                if(i > (users.length - 11)){
                    return <TranslationItem props={{translation, i, users}} />
                }
            }) : ''}
            
        </div>
    )
}

export default Profile