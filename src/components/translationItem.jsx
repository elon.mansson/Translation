import { Button } from "@mantine/core";
import { useDispatch } from "react-redux";
import { updateTranslation } from "../redux/userSlice";

const TranslationItem = (props) => {
    const dispatch = useDispatch()
    //Get the user from localStorage
    const userFromStorage = JSON.parse(localStorage.getItem("my-user"));
    //Create an array with the translation from localstorage
    let newArray = userFromStorage.translations

    //Function to remove translation from localstorage and reduxState. But keep it in the API.
    //Super confusing sentence in the assignment!! v
    //This should “delete” in your API and no longer display on the profile page. To simplify things, you may simply delete the records, but you should NEVER delete data from a database.
    const removeTranslation = (e) => {
        //Get the btn element. Got a little confusing thanks to Mantine button.
        let btn = e.target.parentNode.parentNode;
        //remove the targeted element from the array
        newArray.splice(btn.id, 1);
        //Update the user with the new list
        let updatedUser = { ...userFromStorage, translations: newArray };
        //push the updated user to localstorage
        localStorage.setItem("my-user", JSON.stringify(updatedUser));
        //update the globalstate with the new list.
        dispatch(updateTranslation(updatedUser.translations))
  };

  return (
    <div className="translationItem" key={props.props.i}>
      <p className="translationItemText">{props.props.translation}</p>
      <Button
        className="btn"
        value={props.props.i}
        type="submit"
        id={props.props.i}
        style={{ color: "#003049" }}
        
        sx={() => ({
          background: "#f77f00",
          "&:hover": {
            backgroundColor: "#A55500",
          },
        })}
        onClick={(e) => removeTranslation(e)}
      >
        Delete
      </Button>
    </div>
  );
};

export default TranslationItem;
