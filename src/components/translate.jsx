import { Button, Input } from "@mantine/core";
import { useForm } from "react-hook-form";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addUserTranslation, updateStatus, updateTranslation } from "../redux/userSlice";

//A function to import all images as a single object
function importAll(r) {
  let images = {};
  r.keys().forEach((item, index) => {
    images[item.replace("./", "")] = r(item);
  });
  return images;
}

const images = importAll(
  require.context("../assets/individial_signs", false, /\.(png|jpe?g|svg)$/)
);

const Translate = () => {
  const dispatch = useDispatch();
  const [stringConvert, setStringConver] = useState(["h","e","l","l","o"]);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    //Convert the string into an array
    setStringConver(Array.from(data.translation));
    //get the curent user from localstorage
    let currentUser = JSON.parse(localStorage.getItem("my-user"));
    //update the currentUser with the new string for translation
    currentUser.translations.push(data.translation);
    //update localstorage wite the updated user
    localStorage.setItem("my-user", JSON.stringify(currentUser));
    //Call API patch to push the new object to the API
    await dispatch(addUserTranslation(currentUser));
    //Refetch the API (mostly to see if it works as intended)
    dispatch(updateStatus());
    dispatch(updateTranslation(currentUser.translations))
  };

  return (
    <div className="translate-container hero-component">
      <h1 className="title">Translate page</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Input
          icon="@"
          placeholder="You string for translation"
          size="lg"
          
          {...register("translation", {
            required: "A string is required",
            pattern: {
              value: /^[a-zA-Z\s]*$/,
              message: "The string can only contain letters",
            },  
          })}
        ></Input>
        {errors.name ? <div>{errors.translation.message}</div> : null}
        <Button
          className="btn"
          type="submit"
          style={{ color: "#003049" }}
          sx={() => ({
            background: "#f77f00",
            "&:hover": {
              backgroundColor: "#A55500",
            },
          })}
        >
          Translate
        </Button>
      </form>
      <div className="translation-container">
        {/* A map to return each letter into a img */}
        {stringConvert.map((letter, i) => {
            //Check if letter is a space then return a div with same size as the imgs
          if (letter === " ") {
            return <div className="space"/* style={{ height: "80px", width: "80px"}} */></div>;
          }
          return <img key={i} src={images[letter.toLowerCase() + ".png"]}></img>;
        })}
      </div>
    </div>
  );
};

export default Translate;
