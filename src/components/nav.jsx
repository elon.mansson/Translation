import { Link } from "react-router-dom";
import { Button } from "@mantine/core";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { updateLoggedIn, updateTranslation } from "../redux/userSlice";
import cloud from "../assets/Splash.svg";
import Logo from "../assets/Logo.png";

const Nav = (props) => {
  //State to see if menu is open or not
  const [showMenu, setShowMenu] = useState(false);

  const dispatch = useDispatch();

  return (
    <div className="nav-container">
      <div className="logo-container">
        <div className="icon-container">
          <img src={cloud} alt="" />
          <img src={Logo} alt="" />
        </div>
        <h1>Lost in Translation</h1>
      </div>
      {/* Sending props from APP to see if user is logged in or not */}
      {props.loggedIn ? (
        <>
          <Button
            style={{ color: "#003049" }}
            className="menu-btn"
            onClick={() => {
              setShowMenu(!showMenu);
            }}
            sx={() => ({
              background: "#f77f00",
              "&:hover": {
                backgroundColor: "#A55500",
              },
            })}
          >
            Menu
          </Button>
            {/* Check if menu is open or not and return diffrent elements depending on the useState */}
          {showMenu ? (
            <div className="menu-container">
              <Link to="/profile">
                <Button
                  className="nav-btn"
                  onClick={() => {
                    //Small function to change the state of the menu
                    setShowMenu(!showMenu);
                  }}
                  style={{
                    color: "#003049",
                    borderTopLeftRadius: "5px",
                    borderTopRightRadius: "5px",
                    borderBottomLeftRadius: "0",
                    borderBottomRightRadius: "0",
                  }}
                  sx={() => ({
                    background: "#f77f00",
                    "&:hover": {
                      backgroundColor: "#A55500",
                    },
                  })}
                >
                  Profile
                </Button>
              </Link>
              <Link to="/translate">
                <Button
                  className="nav-btn"
                  onClick={() => {
                    //Small function to change the state of the menu
                    setShowMenu(!showMenu);
                  }}
                  style={{
                    color: "#003049",
                    borderRadius: "0",
                  }}
                  sx={() => ({
                    background: "#f77f00",
                    "&:hover": {
                      backgroundColor: "#A55500",
                    },
                  })}
                >
                  Translate
                </Button>
                <Button
                  className="nav-btn"
                  onClick={() => {
                    //Small function to change the state of the menu
                    setShowMenu(!showMenu);
                    //Change the current global logged in state
                    dispatch(updateLoggedIn(false));
                    //Remove the current user from localstorage
                    localStorage.clear();
                    //Remove the current translationList from the reduxState
                    dispatch(updateTranslation([]));
                  }}
                  style={{
                    color: "#003049",
                    borderTopLeftRadius: "0",
                    borderTopRightRadius: "0",
                    borderBottomLeftRadius: "5px",
                    borderBottomRightRadius: "5px",
                  }}
                  sx={() => ({
                    background: "#f77f00",
                    "&:hover": {
                      backgroundColor: "#A55500",
                    },
                  })}
                >
                  Logout
                </Button>
              </Link>
            </div>
          ) : (
            ""
          )}
        </>
      ) : (
        ""
      )}
    </div>
  );
};

export default Nav;
