import { Button, Input } from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux/es/exports";
import { addUser, updateStatus, updateLoggedIn, updateTranslation } from "../redux/userSlice";
import { useNavigate } from "react-router-dom";

const Signup = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  //Save all users from api
  const users = useSelector((state) => state.user.userList);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    //create a new userObj
    const newUser = {
      id: users[users.length - 1].id + 1,
      translations: [],
      username: data.name.toLowerCase(),
    };
    //A failsafe variable used to see if the user exist or not.
    let checkName = false;
    //Map through all users from API
    users.map((e, i) => {
      //Check if there is a user from the API with the same name as the new user.
      if (e.username.toLowerCase() === data.name.toLowerCase() && checkName === false) {
        //update localstorage with the user from API
        localStorage.setItem("my-user", JSON.stringify(e));
        //Update a globalstate in redux to show that user is logged in
        dispatch(updateLoggedIn(true));
        //Popup to welcome the user back agien
        dispatch(updateTranslation(e.translations));
        showNotification({
          title: `Welcome back ${e.username}`,
        });
        //Update failsafe so it will never be false agien so it wont go into the same IF statement agien.
        checkName = true;
        navigate('/translate')
      } else {
        //If the previous check didnt pass then check if its on the last index and our failsafe hasent been changed.
        if (i === users.length - 1 && checkName === false) {
          //Push the new user to the API
          dispatch(addUser(newUser));
          //Update a globalstate in redux to show that user is logged in
          dispatch(updateLoggedIn(true));
          //Push the current new user into localstorage
          localStorage.setItem("my-user", JSON.stringify(newUser));
          //Popup to welcome the new user
          showNotification({
            title: "New user added",
            message: `Welcome to super translator 2.0 ${newUser.username}`,
          });
          //function to trigger a new API call to fetch all users. (Mostly to control that all works)
          dispatch(updateStatus());
          navigate('/translate')
        }
      }
    });
  };

  return (
    <div className="signup-container hero-component">
      <h1 className="title">Signup page</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Input
          icon="@"
          placeholder="Your email"
          size="lg"
          
          {...register("name", {
            required: "Name is required",
            pattern: {
              value: /^[a-zA-Z]+$/,
              message: "The name can only contain letters"
            }
          })}
        >
      </Input>
          {errors.name ? <div>{errors.name.message}</div> : null}
        <Button
          className="btn"
          type="submit"
          style={{ color: "#003049" }}
          sx={() => ({
            background: "#f77f00",
            "&:hover": {
              backgroundColor: "#A55500",
            },
          })}
        >
          Login / Create user
        </Button>
      </form>
    </div>
  );
};

export default Signup;
